﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class BarraTiempo : MonoBehaviour
{

    public float valor = 100;
    public Image BarraDevida;
    public float tiempo;
    public PlayerController gameOver;
    public bool victoria;
    public GameObject Panel;
    public GameObject Jugador;
    private void Start()
    {
        gameOver = GameObject.FindObjectOfType<PlayerController>();//Esta Linea lo que hace es bucar un objeto con el script"Controller_Hud" y referenciarlo en el inspector
    }
    void Update()
    {

        BarraDevida.fillAmount = valor / 100;//Hacemos uso de la proiedad fillAmount de nuestra imagen y dividimos la cantidad de la variable "Valor" por 100 normalizando la barra  
        if (gameOver.gameOver == false)// usamos la sentencia "if" para preguntar si la propiedad gameOver de la variable gameOver es falsa
        {

            StartCoroutine(Reducir());// invocamos una corrutina llamada "Reducir"





        }
        if (valor <= 0.0f)//Hacemos uso de la sentencia "if" para preguntar si la variable calor es igual o menor de 0.0f
        {
           
                Panel.SetActive(true);
                Time.timeScale = 0.1f;
            Destroy(Jugador);
            if (Input.GetKeyDown(KeyCode.R))
            {

                SceneManager.LoadScene(0);
                Time.timeScale = 1f;
                gameOver.vida = 3;
            }


        }


    }
    IEnumerator Reducir()//Definimos un metodo IEnumerator
    {

        yield return new WaitForSeconds(tiempo);//Indicamos que espere segundos segun la cantidad de la variable "tiempo"
        valor -= Time.deltaTime * 1;// luego de que espere  los segundos indicados igualamos a la resta de valor y time.deltaTime * 3 lo cual aumenta el decremento

    }
}
