﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLaser : MonoBehaviour
{
    public float velocidad;
    public GameObject[] target;
    public int CurPos = 0;
    public int NexPos = 1;
    public bool Push;
    public bool MoveNex;
    public GameObject Punto;
    public int Vueltas;
    void Start()
    {
        target[0] = GameObject.Find("PuntoLaser2");
        target[1] = GameObject.Find("PuntoLaser1");
    }

    // Update is called once per frame
    void Update()
    {
        if (Push == false)
        {
            if (MoveNex)
                transform.position = Vector3.MoveTowards(transform.position, target[NexPos].transform.position, velocidad * Time.deltaTime);
            if (Vector3.Distance(transform.position, target[NexPos].transform.position) <= 0)
            {
                StartCoroutine(MoveTime());
                CurPos = NexPos;
                NexPos++;

                if (NexPos > target.Length - 1)
                {
                    NexPos = 0;
                    Vueltas += 1;
                    if (Vueltas == 1)
                    {
                        Destroy(gameObject);
                    }
                }
            }
        }

    }
    IEnumerator MoveTime()
    {
        MoveNex = false;
        yield return new WaitForSeconds(0.7f);
        MoveNex = true;

    }
}
