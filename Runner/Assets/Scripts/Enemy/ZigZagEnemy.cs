﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZigZagEnemy : MonoBehaviour
{
    public float velocidad;
    public GameObject[] target;
    public int CurPos = 0;
    public int NexPos = 1;
    public bool Push;
    public bool MoveNex;
    public Material Mat1;
    public Material Mat2;
    public Renderer rend;
   
  
    // Start is called before the first frame update
    void Start()
    {
        rend = gameObject.GetComponent<Renderer>();
        target[0] = GameObject.Find("PuntoZig1");
        target[1] = GameObject.Find("PuntoZig2");
        target[2] = GameObject.Find("PuntoZig3");
        target[3] = GameObject.Find("PuntoZig4");
        target[4] = GameObject.Find("PuntoZig5");
        target[5] = GameObject.Find("PuntoZig6");
        if (gameObject.name =="EnemyZigZag2")
        {
            target[0] = GameObject.Find("PuntoZig1 2");
            target[1] = GameObject.Find("PuntoZig2 2");
            target[2] = GameObject.Find("PuntoZig3 2");
            target[3] = GameObject.Find("PuntoZig4 2");
            target[4] = GameObject.Find("PuntoZig5 2");
            target[5] = GameObject.Find("PuntoZig6 2");
        }
        if (gameObject.name == "EnemyZigZag3")
        {
            target[0] = GameObject.Find("PuntoZig1 3");
            target[1] = GameObject.Find("PuntoZig2 3");
            target[2] = GameObject.Find("PuntoZig3 3");
            target[3] = GameObject.Find("PuntoZig4 3");
            target[4] = GameObject.Find("PuntoZig5 3");
            target[5] = GameObject.Find("PuntoZig6 3");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Push == false)
        {
            if (MoveNex)
                transform.position = Vector3.MoveTowards(transform.position, target[NexPos].transform.position, velocidad * Time.deltaTime);
            if (Vector3.Distance(transform.position, target[NexPos].transform.position) <= 0)
            {
                StartCoroutine(MoveTime());
                CurPos = NexPos;
                NexPos++;

                if (NexPos > target.Length - 1)
                {
                    NexPos = 0;
                }
            }
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Base"))
        {
            Destroy(gameObject);
            Destroy(other.gameObject);
        }
    }
    IEnumerator MoveTime()
    {
        MoveNex = false;
        rend.material = Mat1;
      
        gameObject.tag = "Enemy";
        yield return new WaitForSeconds(0.7f);
        gameObject.tag = "Fantasma";
        rend.material = Mat2;
        MoveNex = true;

    }
}

