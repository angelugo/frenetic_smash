﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPlataforma : MonoBehaviour
{
    public float velocidad;
    public GameObject[] target;
    public int CurPos = 0;
    public int NexPos = 1;
    public bool Push;
    public GameObject Final;
    void Start()
    {
        if (gameObject.name == "Plataforma")
        {
            target[0] = GameObject.Find("PuntoPlataforma");
            
            

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Push == false && target[0] != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, target[NexPos].transform.position, velocidad * Time.deltaTime);
            if (Vector3.Distance(transform.position, target[NexPos].transform.position) <= 0)
            {

                CurPos = NexPos;
                NexPos++;

                if (NexPos > target.Length - 1)
                {
                    NexPos = 0;
                }
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
       
        if (other.gameObject.CompareTag("Final"))
        {
            Destroy(gameObject);

        }
    }
}
