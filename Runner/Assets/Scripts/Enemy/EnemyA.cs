﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyA : MonoBehaviour
{
    public float velocidad;
    public GameObject[] target;
    public int CurPos = 0;
    public int NexPos = 1;
    public bool Push;
    public GameObject Final;
    void Start()
    {
        Final = GameObject.Find("Final");
        if (gameObject.name == "EnemyA")
        {
            target[0] = GameObject.Find("Base1");
            if (target[0] == null)
            {
                target[0] = GameObject.Find("Base2");
                if (target[0] == null)
                {
                    target[0] = GameObject.Find("Base3");
                }
            }
           
        }
        if (gameObject.name == "EnemyB")
        {
            target[0] = GameObject.Find("Base2");
            if (target[0] == null)
            {
                target[0] = GameObject.Find("Base1");
                if (target[0] == null)
                {
                    target[0] = GameObject.Find("Base3");
                }
            }
            
        }
        if (gameObject.name == "EnemyC")
        {
            target[0] = GameObject.Find("Base3");
            if (target[0] == null)
            {
                target[0] = GameObject.Find("Base2");
                if (target[0] == null)
                {
                    target[0] = GameObject.Find("Base1");
                }
            }
           
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.name == "EnemyA")
        {
            target[0] = GameObject.Find("Base1");
            if (target[0] == null)
            {
                target[0] = GameObject.Find("Base2");
                if (target[0] == null)
                {
                    target[0] = GameObject.Find("Base3");
                }
            }
          
        }
        if (gameObject.name == "EnemyB")
        {
            target[0] = GameObject.Find("Base2");
            if (target[0] == null)
            {
                target[0] = GameObject.Find("Base1");
                if (target[0] == null)
                {
                    target[0] = GameObject.Find("Base3");
                }
            }
        }
        if (gameObject.name == "EnemyC")
        {
            target[0] = GameObject.Find("Base3");
            if (target[0] == null)
            {
                target[0] = GameObject.Find("Base2");
                if (target[0] == null)
                {
                    target[0] = GameObject.Find("Base1");
                }
            }
        }
        if (Push == false && target[0] != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, target[NexPos].transform.position, velocidad * Time.deltaTime);
            if (Vector3.Distance(transform.position, target[NexPos].transform.position) <= 0)
            {

                CurPos = NexPos;
                NexPos++;

                if (NexPos > target.Length - 1)
                {
                    NexPos = 0;
                }
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, Final.transform.position, velocidad * Time.deltaTime);
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Base"))
        {
            Destroy(gameObject);
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Final"))
        {
            Destroy(gameObject);

        }
    }
}
