﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DivideEnemy : MonoBehaviour
{
    public float velocidad;
    public GameObject[] target;
    public int CurPos = 0;
    public int NexPos = 1;
    public bool Push;
    public int vida;
    public int NumeroDeEnemigosLimite;
    public int numeroDeEnemigos;
    public int NumeroDivide;
    public int NumeroDividir;
    public Transform firePoint;
    public GameObject DoblePrefab;
    public GameObject Player;
    public bool Set;
    public GameObject Final;
    // Start is called before the first frame update
    void Start()
    {
        Final = GameObject.Find("Final");
        Player = GameObject.Find("Player");
        target[0] = GameObject.Find("Base1");
     
        if (gameObject.name == "EnemyDivide2")
        {
            target[0] = GameObject.Find("Base2");
       
        }
        if (gameObject.name == "EnemyDivide3")
        {
            target[0] = GameObject.Find("Base3");
          
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (gameObject.name == "EnemyDivide")
        {
            target[0] = GameObject.Find("Base1");
            if (target[0]== null)
            {
                target[0] = GameObject.Find("Base2");
                if (target[0] == null)
                {
                    target[0] = GameObject.Find("Base3");
                }
            }
        }
        

        if (gameObject.name == "EnemyDivide2")
        {
            target[0] = GameObject.Find("Base2");
            if (target[0] == null)
            {
                target[0] = GameObject.Find("Base3");
                if (target[0] == null)
                {
                    target[0] = GameObject.Find("Base1");
                }
            }

        }
        if (gameObject.name == "EnemyDivide3")
        {
            target[0] = GameObject.Find("Base3");
            if (target[0] == null)
            {
                target[0] = GameObject.Find("Base2");
                if (target[0] == null)
                {
                    target[0] = GameObject.Find("Base1");
                }
            }

        }
        if (Push == false&&target[0] !=null)
        {
            transform.position = Vector3.MoveTowards(transform.position, target[NexPos].transform.position, velocidad * Time.deltaTime);
            if (Vector3.Distance(transform.position, target[NexPos].transform.position) <= 0)
            {

                CurPos = NexPos;
                NexPos++;

                if (NexPos > target.Length - 1)
                {
                    NexPos = 0;
                }
               
            }

        }
        else
        {
              transform.position = Vector3.MoveTowards(transform.position, Final.transform.position, velocidad * Time.deltaTime);
        }
         
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Base"))
        {
            Destroy(gameObject);
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Final"))
        {
            Destroy(gameObject);
           
        }
        if (other.gameObject.CompareTag("Ataque"))
        {
            Set = true;
            if (Set == true && numeroDeEnemigos < NumeroDeEnemigosLimite)
            {
                numeroDeEnemigos += 1;
                NumeroDivide += 1;
                GameObject Doble = Instantiate(DoblePrefab, firePoint.position, firePoint.rotation);
            }
        }
    }
}
