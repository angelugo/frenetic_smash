﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour
{
    public Material Mat1;
    public Material Mat2;
    public Renderer rend;
    public float tiempo;
    public bool gameOver;
    public float descansoTime;
    public bool modoAtaque;
    public bool DescansoE;
    public bool inicio;
    public Transform start;
    public Vector3 Point;
    public Ray camaraRay;
    public int vida=3;
    public int VidaEnemigo;
    public GameObject Big;
    public bool restar;

    public Vida Vida;

    public void Start()
    {
        rend = gameObject.GetComponent<Renderer>();
        Big = GameObject.Find("BigEnemy1");
        vida = 3;
        StartCoroutine(Inicio());
    
        
    }
    private void Update()
    {
        vida = Mathf.Clamp(vida, 0, 3);
        Plane plane1 = new Plane(Vector3.up, Vector3.zero);
    camaraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        float RaycastLargo;
        if (inicio == false)
        {
            if (plane1.Raycast(camaraRay, out RaycastLargo))
            {
                Point = camaraRay.GetPoint(RaycastLargo);
                Debug.DrawLine(camaraRay.origin, Point, Color.blue);

                transform.LookAt(new Vector3(Point.x, transform.position.y, Point.z));
                transform.position = (new Vector3(Point.x, transform.position.y, Point.z));
            }
            if (Input.GetMouseButton(0) && modoAtaque == false)
            {
                if (DescansoE == false)
                {
                    rend.material = Mat2;
                    StartCoroutine(Espera());
                }

            }
         

        }
        if (vida == 0)
        {
            gameOver = true;
            Destroy(gameObject);



        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            SceneManager.LoadScene(0);
            vida = 3;
            Time.timeScale = 1f;
        }
      



    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Laser"))
        {
            if (gameObject.tag == "Ataque")
            {

                Destroy(other.gameObject);
                
            }
            else
            {
                if (vida > -0)
                {
                    vida -= 1;
                    Vida.CambiarSprite(vida);
                }
                StartCoroutine(Inicio());
                gameObject.transform.position = start.transform.position;
            }
        }
        if (other.gameObject.CompareTag("Hexa"))
        {
            if (gameObject.tag == "Jugador")
            {
                StartCoroutine(Inicio());
                gameObject.transform.position = start.transform.position;
                if (vida > -0)
                {
                    vida -= 1;
                    Vida.CambiarSprite(vida);
                }
            }

        }
       
            if (other.gameObject.CompareTag("Plataforma"))
            {

            StartCoroutine(Inicio());
            gameObject.transform.position = start.transform.position;
            if (vida > -0)
            {
                vida -= 1;
                Vida.CambiarSprite(vida);
            }
        }
        
        if (other.gameObject.CompareTag("Wall"))
        {

            if (vida > -0)
            {
                vida -= 1;
                Vida.CambiarSprite(vida);
            }

            StartCoroutine(Inicio());
            gameObject.transform.position = start.transform.position;
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (gameObject.tag == "Ataque")
            {
                if (other.gameObject.name=="BigEnemy1")
                {
                   
                    
                        VidaEnemigo -= 1;
          
                    if (VidaEnemigo == 0)
                    {
                        Destroy(other.gameObject);
                    }
                }
                else
                {
                    Destroy(other.gameObject);
                }
              
            }
            else
            {
                if (vida > -0)
                {
                    vida -= 1;
                    Vida.CambiarSprite(vida);
                }
                StartCoroutine(Inicio());
                gameObject.transform.position = start.transform.position;

            }
        }
    }
    private void OnMouseDown()
    {
    
    }
    public IEnumerator Espera()
    {
       
        modoAtaque = true;
        gameObject.tag = "Ataque";
        yield return new WaitForSeconds(tiempo);
        modoAtaque = false;
        gameObject.tag = "Jugador";

        rend.material = Mat1;
        StartCoroutine(Descanso());
    }
    public IEnumerator Descanso()
    {

       DescansoE = true;
       
        yield return new WaitForSeconds(descansoTime);
        DescansoE = false;
   

    }
    public IEnumerator Inicio()
    {

    inicio = true;
        yield return new WaitForSeconds(1);
       

        inicio = false;


    }
   


}
