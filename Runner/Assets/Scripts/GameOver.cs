﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameOver : MonoBehaviour
{
    public GameObject Panel;
    public PlayerController gameOver;
    public new AudioSource audio = new AudioSource();
    public GameObject Base1;
    public GameObject Base2;
    public GameObject Base3;
    public GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player");
        Base1 = GameObject.Find("Base1");
        Base2 = GameObject.Find("Base2");
        Base3 = GameObject.Find("Base3");
        gameOver = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameOver.gameOver == true)
        {
            Panel.SetActive(true);
            Time.timeScale = 0.1f;
            Destroy(Player);
            if (Input.GetKeyDown(KeyCode.R))
            {
                
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                Time.timeScale = 1f;
                gameOver.vida = 3;
            }
         

        }
        if (Base1 == null&& Base2 == null && Base3 == null )
        {
            gameOver.gameOver = true;
        }
    }
}
