﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class AudioController : MonoBehaviour
{
    public new AudioSource audio = new AudioSource();

    public PlayerController gameOver;

    void Start()
    {
        gameOver = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
    
        if (gameOver.gameOver == true)
        {
            audio.pitch = 0.8f;
            audio.volume = 0.182f;
        }
        else
        {
            audio.pitch = 1;
            audio.volume = 0.88F;
        }
    }

 
}
