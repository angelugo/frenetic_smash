﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexagonoController : MonoBehaviour
{
    public int Numero;
    public bool Activo;
    public float tiempo;
    public GameObject Hexa;
    public Transform firePoint;
    public Material Mat1;
    public Material Mat2;
    public Renderer rend;


    // Start is called before the first frame update
    void Start()
    {
        rend = gameObject.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
    
       
        if (Activo==true)
        {
            rend.material = Mat2;
        }
        if (Activo==false)
        {
            rend.material = Mat1;
        }
    
        
    }
    public IEnumerator Espera()
    {


        Activo = true;
        yield return new WaitForSeconds(tiempo);
        Activo = false;
        GameObject refe = Instantiate(Hexa, firePoint.position, firePoint.rotation);
        Destroy(refe, 3);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            Numero = Random.Range(1, 13);
            if ( Numero == 4)
            {
                StartCoroutine(Espera());

            }
        }
    }
}
