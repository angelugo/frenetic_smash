﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class AudioController2 : MonoBehaviour
{
    public new AudioSource audio = new AudioSource();
    public TextMeshProUGUI text;
    public GameObject TextRefe;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (audio.isPlaying == true)
        {
            TextRefe.SetActive(true);
            text.text = "Pause".ToString();
        }
        else
        {
            TextRefe.SetActive(false);

            text.text = "Play".ToString();
        }
    }
    public void StartAudio()
    {
        if (audio.isPlaying == true)
        {
            audio.Pause();
        }
        else
        {
            audio.Play();

        }
    }
    public void Stop()
    {
        audio.Stop();
    }
    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
